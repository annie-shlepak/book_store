# book_store

### Clone the repository
```
git clone https://annie-shlepak@bitbucket.org/annie-shlepak/book_store.git
```
### Create virtual environment (with Python 3.6) and activate it
### Install requirements
```
pip install -r requirements.txt
```
### Migrate the db
```
python manage.py migrate
```
### Load data to db from fixtures
```
python manage.py loaddata initial_data.json
```
### Create user and use your credentials to login to the site
```
django-admin createsuperuser
```
### Run
```
python manage.py runserver
```
### To run a django management command
```
python manage.py display_books order=asc
python manage.py display_books order=desc
```