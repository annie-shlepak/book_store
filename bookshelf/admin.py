from django.contrib import admin

from bookshelf.models import Book, HttpRequest


@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    pass


@admin.register(HttpRequest)
class HttpRequestAdmin(admin.ModelAdmin):
    pass
