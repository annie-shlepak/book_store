from django import forms
from django.forms import SelectDateWidget

from bookshelf.models import Book


class BookForm(forms.ModelForm):
    class Meta:
        model = Book
        fields = '__all__'
        widgets = {
            'publish_date': SelectDateWidget(),
        }
