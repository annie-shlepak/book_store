from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from bookshelf.models import Book


class Command(BaseCommand):
    help = 'Display books ascending or descending'

    def handle(self, *args, **kwargs):
        if kwargs['order'] == 'asc':
            books = Book.objects.all().values('title', 'publish_date').order_by('publish_date')
            print([f"{book['title']}, {book['publish_date']}" for book in books])
        elif kwargs['order'] == 'desc':
            books = Book.objects.all().values('title', 'publish_date').order_by('-publish_date')
            print([f"{book['title']}, {book['publish_date']}" for book in books])
        else:
            raise Exception('Please specify an argument (asc/desc) to display books!')

    def add_arguments(self, parser):
        parser.add_argument('--order', type=str, help='Specify an order')

