from django.contrib.auth.models import User
from django.utils.deprecation import MiddlewareMixin

from bookshelf.models import HttpRequest


class HttpRequestsMiddleware(MiddlewareMixin):

    def process_request(self, request):
        host = request.get_host()
        method = request.method
        uri = request.build_absolute_uri()

        user_agent = request.META.get('HTTP_USER_AGENT', None)
        accept = request.META.get('HTTP_ACCEPT', None)

        web_request = HttpRequest.objects.create(host=host,
                                                 method=method,
                                                 uri=uri,
                                                 user_agent=user_agent,
                                                 accept=accept)
        web_request.save()
        return None

