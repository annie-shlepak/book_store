# Generated by Django 2.1.4 on 2018-12-13 17:09

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bookshelf', '0006_auto_20181212_1813'),
    ]

    operations = [
        migrations.AlterField(
            model_name='book',
            name='publish_date',
            field=models.DateField(default=datetime.datetime(2018, 12, 13, 17, 9, 34, 907181), null=True),
        ),
    ]
