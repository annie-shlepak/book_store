
from django.db import models
from django.urls import reverse
from django.utils import timezone
from isbn_field import ISBNField


class Book(models.Model):
    title = models.CharField(max_length=200, null=False, blank=False)
    author = models.CharField(max_length=200, null=False, blank=False)
    isbn = ISBNField(null=True, blank=False)
    price = models.FloatField(null=False, blank=False)
    publish_date = models.DateField(default=timezone.now(), null=True, blank=False)

    def get_absolute_url(self):
        return reverse('book-detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['title']


class HttpRequest(models.Model):
    time = models.DateTimeField(auto_now_add=True)
    host = models.CharField(max_length=1000, null=True, blank=True)
    method = models.CharField(max_length=50)
    uri = models.CharField(max_length=1500)
    user_agent = models.CharField(max_length=1000, blank=True, null=True)
    accept = models.CharField(max_length=1000, blank=True, null=True)

    class Meta:
        verbose_name = 'Http Request'
        verbose_name_plural = 'Http Requests'
        ordering = ['time']

