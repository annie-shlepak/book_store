from django.urls import path

from bookshelf.views import BookListView, BookDetailView, BookCreateView, BookEditView, BookDeleteView, RequestsListView

urlpatterns = [
    path('', BookListView.as_view(), name='book-list'),
    path('books/<int:pk>/', BookDetailView.as_view(), name='book-detail'),
    path('books/add/', BookCreateView.as_view(), name='book-create'),
    path('books/edit/<int:pk>/', BookEditView.as_view(), name='book-edit'),
    path('books/delete/<int:pk>/', BookDeleteView.as_view(), name='book-delete'),

    path('requests/', RequestsListView.as_view(), name='requests-list')
]
