import logging

from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView, CreateView, DeleteView, UpdateView

from bookshelf.forms import BookForm
from bookshelf.models import Book, HttpRequest

logger = logging.getLogger('__name__')


class BookListView(ListView):
    model = Book


class BookDetailView(DetailView):
    model = Book


@method_decorator(login_required, name='dispatch')
class BookCreateView(CreateView):
    form_class = BookForm
    model = Book
    template_name = 'bookshelf/book_create_update.html'
    success_url = reverse_lazy('book-list')

    def form_valid(self, form):
        book = form.save(commit=False)
        logging.debug(f'CREATED: "{book.title}" authored by {book.author}.')
        return super().form_valid(form)


@method_decorator(login_required, name='dispatch')
class BookEditView(UpdateView):
    form_class = BookForm
    model = Book
    template_name = 'bookshelf/book_create_update.html'
    template_name_suffix = '_update_form'

    def form_valid(self, form):
        is_changed = form.has_changed()
        if is_changed:
            book = form.save(commit=False)
            logging.debug(f'UPDATED: "{book.title}" authored by {book.author} with ID:{book.id}')
        return super().form_valid(form)


@method_decorator(login_required, name='dispatch')
class BookDeleteView(DeleteView):
    model = Book
    success_url = reverse_lazy('book-list')

    def delete(self, request, *args, **kwargs):
        book = self.get_object()
        logging.debug(f'DELETED: "{book.title}" authored by {book.author} with ID:{book.id}')
        return super().delete(request)


class RequestsListView(ListView):
    model = HttpRequest

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['request_list'] = HttpRequest.objects.all().order_by('-time')[:10]
        return context

